<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ip-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ip;

use PhpExtended\Lexer\LexerInterface;
use PhpExtended\Parser\AbstractParserLexer;
use PhpExtended\Parser\ParseException;

/**
 * IpAddressParser class file.
 * 
 * This class is a simple implementation of the IpAddressParserInterface.
 * 
 * @author Anastaszor
 * @extends AbstractParserLexer<Ipv6NetworkInterface>
 */
class Ipv6NetworkParser extends AbstractParserLexer implements Ipv6NetworkParserInterface
{
	
	public const L_DIGITS = 1; // 0-9
	public const L_HEXA = 2; // A-F
	public const L_SCLN = 3; // :
	public const L_SLSH = 4; // /
	public const L_EXPD = 5; // ::
	public const T_IPV6 = 10;
	
	/**
	 * The parser for ipv6s.
	 * 
	 * @var Ipv6AddressParserInterface
	 */
	protected Ipv6AddressParserInterface $_ipv6Parser;
	
	/**
	 * Builds a new Ipv6NetworkParser with the given lexer configuration.
	 * 
	 * @param ?Ipv6AddressParserInterface $ipv6Parser
	 */
	public function __construct(?Ipv6AddressParserInterface $ipv6Parser = null)
	{
		parent::__construct(Ipv6NetworkInterface::class);
		
		if(null === $ipv6Parser)
		{
			$ipv6Parser = new Ipv6AddressParser();
		}
		$this->_ipv6Parser = $ipv6Parser;
		
		$this->_config->addMappings(LexerInterface::CLASS_DIGIT, self::L_DIGITS);
		$this->_config->addMappings('ABCDEFabcdef', self::L_HEXA);
		$this->_config->addMappings(':', self::L_SCLN);
		$this->_config->addMappings('/', self::L_SLSH);
		
		$this->_config->addMerging(self::L_DIGITS, self::L_DIGITS, self::L_DIGITS);
		$this->_config->addMerging(self::L_DIGITS, self::L_HEXA, self::L_HEXA);
		$this->_config->addMerging(self::L_HEXA, self::L_HEXA, self::L_HEXA);
		$this->_config->addMerging(self::L_HEXA, self::L_DIGITS, self::L_HEXA);
		$this->_config->addMerging(self::L_SCLN, self::L_SCLN, self::L_EXPD);
		
		$this->_config->addMerging(self::L_DIGITS, self::L_SCLN, self::T_IPV6);
		$this->_config->addMerging(self::L_HEXA, self::L_SCLN, self::T_IPV6);
		$this->_config->addMerging(self::L_EXPD, self::L_DIGITS, self::T_IPV6);
		$this->_config->addMerging(self::L_EXPD, self::L_HEXA, self::T_IPV6);
		$this->_config->addMerging(self::T_IPV6, self::L_SCLN, self::T_IPV6);
		$this->_config->addMerging(self::T_IPV6, self::L_DIGITS, self::T_IPV6);
		$this->_config->addMerging(self::T_IPV6, self::L_HEXA, self::T_IPV6);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Parser\ParserInterface::parse()
	 */
	public function parse(?string $data) : Ipv6NetworkInterface
	{
		if('localhost' === $data)
		{
			return new Ipv6Network(new Ipv6Address(0, 0, 0, 0, 0, 0, 0, 1), 128);
		}
		
		return parent::parse($data); // php interface compatibility
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Parser\ParserInterface::parse()
	 */
	public function parseLexer(LexerInterface $lexer) : Ipv6NetworkInterface
	{
		$bytes = [];
		$token = null;
		
		$lexer->rewind();
		
		$token = $this->expectOneOf($lexer, [self::T_IPV6, self::L_EXPD], $token);
		
		$ipv6 = $this->_ipv6Parser->parse($token->getData());
		
		$token = $this->expectOneOf($lexer, [self::L_SLSH], $token);
		$token = $this->expectOneOf($lexer, [self::L_DIGITS], $token);
		$mask = (int) $token->getData();
		
		if(128 < $mask)
		{
			$message = 'Unexpected value {value} for token {name} when parsing ipv4 netmask, should be less than or equal to 128';
			$context = [
				'{value}' => $mask,
				'{name}' => $this->getTokenName($token->getCode()),
				'{k}' => \count($bytes),
			];
			
			throw new ParseException($this->_class, $token->getData(), $token->getColumn(), \strtr($message, $context));
		}
		
		$this->expectEof($lexer, $token);
		
		return new Ipv6Network($ipv6, $mask);
	}
	
}
