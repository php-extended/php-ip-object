<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ip-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ip;

use PhpExtended\Lexer\LexemeInterface;
use PhpExtended\Lexer\LexerInterface;
use PhpExtended\Parser\AbstractParserLexer;
use PhpExtended\Parser\ParseException;
use PhpExtended\Parser\ParseThrowable;

/**
 * IpAddressParser class file.
 * 
 * This class is a simple implementation of the IpAddressParserInterface.
 * 
 * @author Anastaszor
 * @extends AbstractParserLexer<Ipv6AddressInterface>
 */
class Ipv6AddressParser extends AbstractParserLexer implements Ipv6AddressParserInterface
{
	
	public const L_HEXA = 1;
	public const L_SCLN = 2;
	public const L_EXPD = -10;
	
	/**
	 * Builds a new Ipv6AddressParser with the given lexer configuration.
	 */
	public function __construct()
	{
		parent::__construct(Ipv6AddressInterface::class);
		$this->_config->addMappings(LexerInterface::CLASS_HEXA, self::L_HEXA);
		$this->_config->addMappings(':', self::L_SCLN);
		
		$this->_config->addMerging(self::L_HEXA, self::L_HEXA, self::L_HEXA);
		$this->_config->addMerging(self::L_SCLN, self::L_SCLN, self::L_EXPD);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Parser\ParserInterface::parse()
	 */
	public function parse(?string $data) : Ipv6AddressInterface
	{
		if('localhost' === $data)
		{
			return new Ipv6Address(0, 0, 0, 0, 0, 0, 0, 1);
		}
		
		return parent::parse($data); // php interface compatibility
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Parser\AbstractParserLexer::parseLexer()
	 */
	public function parseLexer(LexerInterface $lexer) : Ipv6AddressInterface
	{
		$bytes = [];
		$token = null;
		$hasExpander = false;
		
		$lexer->rewind();
		
		// {{{ handle beginning by :: or by hexa
		$token = $this->expectOneOf($lexer, [self::L_HEXA, self::L_EXPD], $token);
		if($token->getCode() === self::L_EXPD)
		{
			$bytes[] = self::L_EXPD;
			$hasExpander = true;
			
			if(!$lexer->valid())
			{
				return new Ipv6Address(0, 0, 0, 0, 0, 0, 0, 0);
			}
			
			$token = $this->expectOneOf($lexer, [self::L_HEXA], $token);
		}
		$bytes[] = $this->getIntval($token, 1);
		// }}}
		
		for($i = 0; 7 > $i; $i++)
		{
			if(!$lexer->valid())
			{
				break;
			}
			
			$expected = $hasExpander ? [self::L_SCLN] : [self::L_SCLN, self::L_EXPD];
			$token = $this->expectOneOf($lexer, $expected, $token);
			if($token->getCode() === self::L_EXPD)
			{
				$bytes[] = self::L_EXPD;
				$hasExpander = true;
				// the next value of valid changed because of expectOneOf
				/** @phpstan-ignore-next-line */
				if(!$lexer->valid()) // end at ::
				{
					break;
				}
			}
			
			$token = $this->expectOneOf($lexer, [self::L_HEXA], $token);
			
			$bytes[] = $this->getIntval($token, \count($bytes));
		}
		
		$this->expectEof($lexer, $token);
		
		$parts = $this->ipv6Expand($bytes);
		
		if(8 > \count($parts))
		{
			$message = 'Failed to find 8 hexadecimal places for an ipv6 address ({k} found).';
			$context = ['{k}' => \count($parts)];
			
			throw new ParseException($this->_class, $token->getData(), $token->getColumn(), \strtr($message, $context));
		}
		
		/** @psalm-suppress PossiblyUndefinedIntArrayOffset */
		return new Ipv6Address(
			$parts[0],
			$parts[1],
			$parts[2],
			$parts[3],
			$parts[4],
			$parts[5],
			$parts[6],
			$parts[7],
		);
	}
	
	/**
	 * Gets the integer value from the hexadecimal data.
	 * 
	 * @param LexemeInterface $token
	 * @param integer $kth the k of the k-th token
	 * @return integer the hexa unscrambled
	 * @throws ParseThrowable
	 */
	public function getIntval(LexemeInterface $token, int $kth) : int
	{
		$intval = (int) \hexdec($token->getData());
		
		if(0xFFFF < $intval)
		{
			$message = 'Unexpected value {value} for token {name} when parsing ipv6 address on byte nb {k}, should be less than or equal to 65535';
			$context = [
				'{value}' => $intval,
				'{name}' => $this->getTokenName($token->getCode()),
				'{k}' => $kth,
			];
			
			throw new ParseException($this->_class, $token->getData(), $token->getColumn(), \strtr($message, $context));
		}
		
		return $intval;
	}
	
	/**
	 * Expants the L_EXPAND token to valid digit lexemes.
	 * 
	 * @param array<integer, integer> $parts
	 * @return array<integer, integer>
	 */
	public function ipv6Expand(array $parts) : array
	{
		$newParts = [];
		
		foreach($parts as $part)
		{
			if(self::L_EXPD !== $part)
			{
				$newParts[] = $part;
				continue;
			}
			
			$qty = 8 - (\count($parts) - 1);
			
			for($qnb = 0; $qnb < $qty; $qnb++)
			{
				$newParts[] = 0;
			}
		}
		
		return $newParts;
	}
	
}
