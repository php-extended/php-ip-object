<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ip-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ip;

use PhpExtended\Lexer\LexerInterface;
use PhpExtended\Parser\AbstractParserLexer;
use PhpExtended\Parser\ParseException;

/**
 * IpAddressParser class file.
 * 
 * This class is a simple implementation of the IpAddressParserInterface.
 * 
 * @author Anastaszor
 * @extends AbstractParserLexer<Ipv4NetworkInterface>
 */
class Ipv4NetworkParser extends AbstractParserLexer implements Ipv4NetworkParserInterface
{
	
	public const L_DIGITS = 1;
	public const L_DOT = 2;
	public const L_SLASH = 3;
	public const T_IPV4 = 10;
	
	/**
	 * The parser for ipv4s.
	 * 
	 * @var Ipv4AddressParserInterface
	 */
	protected Ipv4AddressParserInterface $_ipv4Parser;
	
	/**
	 * Builds a new Ipv6AddressParser with the given lexer configuration.
	 * 
	 * @param ?Ipv4AddressParserInterface $ipv4Parser
	 */
	public function __construct(?Ipv4AddressParserInterface $ipv4Parser = null)
	{
		parent::__construct(Ipv4NetworkInterface::class);
		
		if(null === $ipv4Parser)
		{
			$ipv4Parser = new Ipv4AddressParser();
		}
		$this->_ipv4Parser = $ipv4Parser;
		
		$this->_config->addMappings(LexerInterface::CLASS_DIGIT, self::L_DIGITS);
		$this->_config->addMappings('.', self::L_DOT);
		$this->_config->addMappings('/', self::L_SLASH);
		
		$this->_config->addMerging(self::L_DIGITS, self::L_DIGITS, self::L_DIGITS);
		$this->_config->addMerging(self::L_DIGITS, self::L_DOT, self::T_IPV4);
		$this->_config->addMerging(self::T_IPV4, self::L_DIGITS, self::T_IPV4);
		$this->_config->addMerging(self::T_IPV4, self::L_DOT, self::T_IPV4);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Parser\ParserInterface::parse()
	 */
	public function parse(?string $data) : Ipv4NetworkInterface
	{
		if('localhost' === $data)
		{
			return new Ipv4Network(new Ipv4Address(127, 0, 0, 1), 32);
		}
		
		return parent::parse($data); // php interface compatibility
	}
	
	/**
	 * @param LexerInterface $lexer
	 * @return Ipv4NetworkInterface
	 */
	public function parseLexer(LexerInterface $lexer) : Ipv4NetworkInterface
	{
		$bytes = [];
		$token = null;
		
		$lexer->rewind();
		
		$token = $this->expectOneOf($lexer, [self::T_IPV4], $token);
		
		$ipv4 = $this->_ipv4Parser->parse($token->getData());
		
		$token = $this->expectOneOf($lexer, [self::L_SLASH], $token);
		$token = $this->expectOneOf($lexer, [self::L_DIGITS], $token);
		$mask = (int) $token->getData();
		
		if(32 < $mask)
		{
			$message = 'Unexpected value {value} for token {name} when parsing ipv4 netmask, should be less than or equal to 32';
			$context = [
				'{value}' => $mask,
				'{name}' => $this->getTokenName($token->getCode()),
				'{k}' => \count($bytes),
			];
			
			throw new ParseException($this->_class, $token->getData(), $token->getColumn(), \strtr($message, $context));
		}
		
		$this->expectEof($lexer, $token);
		
		return new Ipv4Network($ipv4, $mask);
	}
	
}
