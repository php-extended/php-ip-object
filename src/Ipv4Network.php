<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ip-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ip;

/**
 * Ipv4Network class file.
 * 
 * This class is a simple implementation of the Ipv4NetworkInterface.
 * 
 * @author Anastaszor
 */
class Ipv4Network implements Ipv4NetworkInterface
{
	
	/**
	 * The ip address at the start of the network.
	 * 
	 * @var Ipv4AddressInterface
	 */
	protected Ipv4AddressInterface $_ip;
	
	/**
	 * The mask.
	 * 
	 * @var integer
	 */
	protected int $_mask = 0;
	
	/**
	 * Builds a new Ipv4Network with the given bytes and mask.
	 * 
	 * @param Ipv4AddressInterface $ipAddress
	 * @param integer $mask
	 */
	public function __construct(Ipv4AddressInterface $ipAddress, int $mask)
	{
		$this->_mask = (0 >= $mask) ? 0 : (($mask - 1) & 31) + 1;
		$bitmask = \str_repeat('1', $this->_mask).\str_repeat('0', 32 - $this->_mask);
		$mask1 = (int) \bindec((string) \mb_substr($bitmask, 0, 8));
		$mask2 = (int) \bindec((string) \mb_substr($bitmask, 8, 8));
		$mask3 = (int) \bindec((string) \mb_substr($bitmask, 16, 8));
		$mask4 = (int) \bindec((string) \mb_substr($bitmask, 24, 8));
		$this->_ip = new Ipv4Address(
			$ipAddress->getFirstByte() & $mask1,
			$ipAddress->getSecondByte() & $mask2,
			$ipAddress->getThirdByte() & $mask3,
			$ipAddress->getFourthByte() & $mask4,
		);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return $this->getCanonicalRepresentation();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ip\Ipv4NetworkInterface::getStartIp()
	 */
	public function getStartIp() : Ipv4AddressInterface
	{
		return $this->_ip;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ip\Ipv4NetworkInterface::getEndIp()
	 */
	public function getEndIp() : Ipv4AddressInterface
	{
		return $this->_ip->add($this->getWildmaskIp());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ip\Ipv4NetworkInterface::getNetworkIp()
	 */
	public function getNetworkIp() : Ipv4AddressInterface
	{
		return $this->getStartIp();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ip\Ipv4NetworkInterface::getNetmaskIp()
	 */
	public function getNetmaskIp() : Ipv4AddressInterface
	{
		$bitmask = \str_repeat('1', $this->_mask).\str_repeat('0', 32 - $this->_mask);
		$mk1 = (int) \bindec((string) \mb_substr($bitmask, 0, 8));
		$mk2 = (int) \bindec((string) \mb_substr($bitmask, 8, 8));
		$mk3 = (int) \bindec((string) \mb_substr($bitmask, 16, 8));
		$mk4 = (int) \bindec((string) \mb_substr($bitmask, 24, 8));
		
		return new Ipv4Address($mk1, $mk2, $mk3, $mk4);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ip\Ipv4NetworkInterface::getWildmaskIp()
	 */
	public function getWildmaskIp() : Ipv4AddressInterface
	{
		return $this->getNetmaskIp()->bitwiseNot();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ip\Ipv4NetworkInterface::getMaskBits()
	 */
	public function getMaskBits() : int
	{
		return $this->_mask;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ip\Ipv4NetworkInterface::getGatewayIp()
	 */
	public function getGatewayIp() : Ipv4AddressInterface
	{
		return $this->getStartIp()->add(new Ipv4Address(0, 0, 0, 1));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ip\Ipv4NetworkInterface::getBroadcastIp()
	 */
	public function getBroadcastIp() : Ipv4AddressInterface
	{
		return $this->getEndIp()->substract(new Ipv4Address(0, 0, 0, 1));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ip\Ipv4NetworkInterface::getNumberOfAddresses()
	 */
	public function getNumberOfAddresses() : int
	{
		return (2 ** (32 - $this->getMaskBits())) - 2;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ip\Ipv4NetworkInterface::getNetworkClass()
	 */
	public function getNetworkClass() : string
	{
		$byte = $this->getStartIp()->getFirstByte();
		if(0 === ($byte & 128))
		{
			return 'A';
		}
		if(128 === ($byte & 192))
		{
			return 'B';
		}
		if(192 === ($byte & 224))
		{
			return 'C';
		}
		if(224 === ($byte & 240))
		{
			return 'D';
		}
		
		return 'E';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ip\Ipv4NetworkInterface::equals()
	 */
	public function equals($other) : bool
	{
		return $other instanceof Ipv4NetworkInterface
			&& $this->getMaskBits() === $other->getMaskBits()
			&& $this->getStartIp()->equals($other->getStartIp());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ip\Ipv4NetworkInterface::containsAddress()
	 */
	public function containsAddress(Ipv4AddressInterface $address) : bool
	{
		return $address->bitwiseAnd($this->getNetmaskIp())->equals($this->getNetworkIp());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ip\Ipv4NetworkInterface::containsNetwork()
	 */
	public function containsNetwork(Ipv4NetworkInterface $subnetwork) : bool
	{
		return $this->containsAddress($subnetwork->getStartIp())
			&& $this->containsAddress($subnetwork->getEndIp());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ip\Ipv4NetworkInterface::absorbAddress()
	 */
	public function absorbAddress(Ipv4AddressInterface $address) : Ipv4NetworkInterface
	{
		if($this->containsAddress($address))
		{
			return $this;
		}
		
		return (new self($this->_ip, $this->_mask - 1))
			->absorbAddress($address)
		;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ip\Ipv4NetworkInterface::absorbNetwork()
	 */
	public function absorbNetwork(Ipv4NetworkInterface $network) : Ipv4NetworkInterface
	{
		return $this->absorbAddress($network->getStartIp())->absorbAddress($network->getEndIp());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ip\Ipv4NetworkInterface::getCanonicalRepresentation()
	 */
	public function getCanonicalRepresentation() : string
	{
		return $this->_ip->getCanonicalRepresentation().'/'.((string) $this->_mask);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ip\Ipv4NetworkInterface::toArray()
	 */
	public function toArray() : array
	{
		return \array_merge($this->_ip->toArray(), [$this->_mask]);
	}
	
}
