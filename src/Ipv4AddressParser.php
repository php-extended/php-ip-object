<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ip-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ip;

use PhpExtended\Lexer\LexerInterface;
use PhpExtended\Parser\AbstractParserLexer;
use PhpExtended\Parser\ParseException;

/**
 * IpAddressParser class file.
 * 
 * This class is a simple implementation of the IpAddressParserInterface.
 * 
 * @author Anastaszor
 * @extends AbstractParserLexer<Ipv4AddressInterface>
 */
class Ipv4AddressParser extends AbstractParserLexer implements Ipv4AddressParserInterface
{
	
	public const L_DIGITS = 1;
	public const L_DOT = 2;
	
	/**
	 * Builds a new Ipv4AddressParser with the given lexer configuration.
	 */
	public function __construct()
	{
		parent::__construct(Ipv4AddressInterface::class);
		$this->_config->addMappings(LexerInterface::CLASS_DIGIT, self::L_DIGITS);
		$this->_config->addMappings('.', self::L_DOT);
		
		$this->_config->addMerging(self::L_DIGITS, self::L_DIGITS, self::L_DIGITS);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Parser\ParserInterface::parse()
	 */
	public function parse(?string $data) : Ipv4AddressInterface
	{
		if('localhost' === $data)
		{
			return new Ipv4Address(127, 0, 0, 1);
		}
		
		return parent::parse($data); // php interface compatibility
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Parser\AbstractParserLexer::parseLexer()
	 */
	public function parseLexer(LexerInterface $lexer) : Ipv4AddressInterface
	{
		$bytes = [];
		$token = null;
		
		$lexer->rewind();
		
		for($i = 0; 4 > $i; $i++)
		{
			$token = $this->expectOneOf($lexer, [self::L_DIGITS], $token);
			$intval = (int) $token->getData();
			
			if(255 < $intval)
			{
				$message = 'Unexpected value {value} for token {name} when parsing ipv4 address on byte nb {k}, should be less than or equal to 255';
				$context = [
					'{value}' => $intval,
					'{name}' => $this->getTokenName($token->getCode()),
					'{k}' => \count($bytes),
				];
				
				throw new ParseException($this->_class, $token->getData(), $token->getColumn(), \strtr($message, $context));
			}
			$bytes[] = $intval;
			
			if(\count($bytes) === 4)
			{
				break;
			}
			
			$token = $this->expectOneOf($lexer, [self::L_DOT], $token);
		}
		
		$this->expectEof($lexer, $token);
		
		/** @psalm-suppress PossiblyUndefinedArrayOffset */
		return new Ipv4Address($bytes[0], $bytes[1], $bytes[2], $bytes[3]);
	}
	
}
