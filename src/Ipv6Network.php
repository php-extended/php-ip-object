<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ip-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ip;

/**
 * Ipv6Network class file.
 * 
 * This class is a simple implementation of the Ipv6NetworkInterface.
 * 
 * @author anastaszor
 */
class Ipv6Network implements Ipv6NetworkInterface
{
	
	/**
	 * The ip address at the start of the network.
	 * 
	 * @var Ipv6AddressInterface
	 */
	protected Ipv6AddressInterface $_ip;
	
	/**
	 * The mask.
	 * 
	 * @var integer
	 */
	protected int $_mask = 0;
	
	/**
	 * Builds a new Ipv6Network with the given bytes and mask.
	 * 
	 * @param Ipv6AddressInterface $ipAddress
	 * @param integer $mask
	 */
	public function __construct(Ipv6AddressInterface $ipAddress, int $mask)
	{
		$this->_mask = (0 >= $mask) ? 0 : (($mask - 1) & 127) + 1;
		$bitmask = \str_repeat('1', $this->_mask).\str_repeat('0', 128 - $this->_mask);
		$mask1 = (int) \bindec((string) \mb_substr($bitmask, 0, 16));
		$mask2 = (int) \bindec((string) \mb_substr($bitmask, 16, 16));
		$mask3 = (int) \bindec((string) \mb_substr($bitmask, 32, 16));
		$mask4 = (int) \bindec((string) \mb_substr($bitmask, 48, 16));
		$mask5 = (int) \bindec((string) \mb_substr($bitmask, 64, 16));
		$mask6 = (int) \bindec((string) \mb_substr($bitmask, 80, 16));
		$mask7 = (int) \bindec((string) \mb_substr($bitmask, 96, 16));
		$mask8 = (int) \bindec((string) \mb_substr($bitmask, 112, 16));
		$this->_ip = new Ipv6Address(
			$ipAddress->getFirstGroup() & $mask1,
			$ipAddress->getSecondGroup() & $mask2,
			$ipAddress->getThirdGroup() & $mask3,
			$ipAddress->getFourthGroup() & $mask4,
			$ipAddress->getFifthGroup() & $mask5,
			$ipAddress->getSixthGroup() & $mask6,
			$ipAddress->getSeventhGroup() & $mask7,
			$ipAddress->getEighthGroup() & $mask8,
		);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return $this->getCanonicalRepresentation();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ip\Ipv6NetworkInterface::getStartIp()
	 */
	public function getStartIp() : Ipv6AddressInterface
	{
		return $this->_ip;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ip\Ipv6NetworkInterface::getEndIp()
	 */
	public function getEndIp() : Ipv6AddressInterface
	{
		return $this->_ip->add($this->getWildmaskIp());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ip\Ipv6NetworkInterface::getNetworkIp()
	 */
	public function getNetworkIp() : Ipv6AddressInterface
	{
		return $this->getStartIp();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ip\Ipv6NetworkInterface::getNetmaskIp()
	 */
	public function getNetmaskIp() : Ipv6AddressInterface
	{
		$bitmask = \str_repeat('1', $this->_mask).\str_repeat('0', 128 - $this->_mask);
		$mk1 = (int) \bindec((string) \mb_substr($bitmask, 0, 16));
		$mk2 = (int) \bindec((string) \mb_substr($bitmask, 16, 16));
		$mk3 = (int) \bindec((string) \mb_substr($bitmask, 32, 16));
		$mk4 = (int) \bindec((string) \mb_substr($bitmask, 48, 16));
		$mk5 = (int) \bindec((string) \mb_substr($bitmask, 64, 16));
		$mk6 = (int) \bindec((string) \mb_substr($bitmask, 80, 16));
		$mk7 = (int) \bindec((string) \mb_substr($bitmask, 96, 16));
		$mk8 = (int) \bindec((string) \mb_substr($bitmask, 112, 16));
		
		return new Ipv6Address(
			$this->_ip->getFirstGroup() | $mk1,
			$this->_ip->getSecondGroup() | $mk2,
			$this->_ip->getThirdGroup() | $mk3,
			$this->_ip->getFourthGroup() | $mk4,
			$this->_ip->getFifthGroup() | $mk5,
			$this->_ip->getSixthGroup() | $mk6,
			$this->_ip->getSeventhGroup() | $mk7,
			$this->_ip->getEighthGroup() | $mk8,
		);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ip\Ipv6NetworkInterface::getWildmaskIp()
	 */
	public function getWildmaskIp() : Ipv6AddressInterface
	{
		return $this->getNetmaskIp()->bitwiseNot();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ip\Ipv6NetworkInterface::getMaskBits()
	 */
	public function getMaskBits() : int
	{
		return $this->_mask;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ip\Ipv6NetworkInterface::getGatewayIp()
	 */
	public function getGatewayIp() : Ipv6AddressInterface
	{
		return $this->getStartIp()->add(new Ipv6Address(0, 0, 0, 0, 0, 0, 0, 1));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ip\Ipv6NetworkInterface::getBroadcastIp()
	 */
	public function getBroadcastIp() : Ipv6AddressInterface
	{
		return $this->getEndIp()->substract(new Ipv6Address(0, 0, 0, 0, 0, 0, 0, 1));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ip\Ipv6NetworkInterface::getNumberOfAddresses()
	 */
	public function getNumberOfAddresses() : int
	{
		return (2 ** (128 - $this->getMaskBits())) - 2;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ip\Ipv6NetworkInterface::equals()
	 */
	public function equals($other) : bool
	{
		return $other instanceof Ipv6NetworkInterface
			&& $this->getStartIp()->equals($other->getStartIp())
			&& $this->getEndIp()->equals($other->getEndIp());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ip\Ipv6NetworkInterface::containsAddress()
	 */
	public function containsAddress(Ipv6AddressInterface $address) : bool
	{
		return $address->bitwiseAnd($this->getNetmaskIp())->equals($this->getNetworkIp());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ip\Ipv6NetworkInterface::containsNetwork()
	 */
	public function containsNetwork(Ipv6NetworkInterface $subnetwork) : bool
	{
		return $this->containsAddress($subnetwork->getStartIp())
			&& $this->containsAddress($subnetwork->getEndIp());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ip\Ipv6NetworkInterface::absorbAddress()
	 */
	public function absorbAddress(Ipv6AddressInterface $address) : Ipv6NetworkInterface
	{
		if($this->containsAddress($address))
		{
			return $this;
		}
		
		return (new self($this->_ip, $this->_mask - 1))
			->absorbAddress($address)
		;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ip\Ipv6NetworkInterface::absorbNetwork()
	 */
	public function absorbNetwork(Ipv6NetworkInterface $network) : Ipv6NetworkInterface
	{
		return $this->absorbAddress($network->getStartIp())->absorbAddress($network->getEndIp());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ip\Ipv6NetworkInterface::getCanonicalRepresentation()
	 */
	public function getCanonicalRepresentation() : string
	{
		return $this->_ip->getCanonicalRepresentation().'/'.((string) $this->_mask);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ip\Ipv6NetworkInterface::toArray()
	 */
	public function toArray() : array
	{
		return \array_merge($this->_ip->toArray(), [$this->_mask]);
	}
	
}
