<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ip-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ip;

/**
 * Ipv4Address class file.
 *
 * This class is a simple implementation of the Ipv4AddressInterface.
 *
 * @author Anastaszor
 */
class Ipv4Address implements Ipv4AddressInterface
{
	
	/**
	 * The first byte.
	 * 
	 * @var integer
	 */
	protected int $_b1 = 0;
	
	/**
	 * The second byte.
	 * 
	 * @var integer
	 */
	protected int $_b2 = 0;
	
	/**
	 * The third byte.
	 * 
	 * @var integer
	 */
	protected int $_b3 = 0;
	
	/**
	 * The fourth byte.
	 * 
	 * @var integer
	 */
	protected int $_b4 = 0;
	
	/**
	 * Builds a new Ipv4Address with the given bytes.
	 * 
	 * @param integer $bit1
	 * @param integer $bit2
	 * @param integer $bit3
	 * @param integer $bit4
	 */
	public function __construct(int $bit1, int $bit2, int $bit3, int $bit4)
	{
		$this->_b1 = $bit1 & 0x000000FF;
		$this->_b2 = $bit2 & 0x000000FF;
		$this->_b3 = $bit3 & 0x000000FF;
		$this->_b4 = $bit4 & 0x000000FF;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return $this->getCanonicalRepresentation();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ip\Ipv4AddressInterface::getFirstByte()
	 */
	public function getFirstByte() : int
	{
		return $this->_b1;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ip\Ipv4AddressInterface::getSecondByte()
	 */
	public function getSecondByte() : int
	{
		return $this->_b2;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ip\Ipv4AddressInterface::getThirdByte()
	 */
	public function getThirdByte() : int
	{
		return $this->_b3;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ip\Ipv4AddressInterface::getFourthByte()
	 */
	public function getFourthByte() : int
	{
		return $this->_b4;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ip\Ipv4AddressInterface::getSignedValue()
	 */
	public function getSignedValue() : int
	{
		return ($this->_b1 << 24) | ($this->_b2 << 16) | ($this->_b3 << 8) | $this->_b4;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ip\Ipv4AddressInterface::getShortRepresentation()
	 */
	public function getShortRepresentation() : string
	{
		return $this->getCanonicalRepresentation();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ip\Ipv4AddressInterface::getCanonicalRepresentation()
	 */
	public function getCanonicalRepresentation() : string
	{
		return ((string) $this->_b1)
			.'.'.((string) $this->_b2)
			.'.'.((string) $this->_b3)
			.'.'.((string) $this->_b4);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ip\Ipv4AddressInterface::equals()
	 */
	public function equals($object) : bool
	{
		return $object instanceof Ipv4AddressInterface
			&& $this->getFirstByte() === $object->getFirstByte()
			&& $this->getSecondByte() === $object->getSecondByte()
			&& $this->getThirdByte() === $object->getThirdByte()
			&& $this->getFourthByte() === $object->getFourthByte();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ip\Ipv4AddressInterface::bitwiseAnd()
	 */
	public function bitwiseAnd(Ipv4AddressInterface $other) : Ipv4AddressInterface
	{
		return new self(
			$this->getFirstByte() & $other->getFirstByte(),
			$this->getSecondByte() & $other->getSecondByte(),
			$this->getThirdByte() & $other->getThirdByte(),
			$this->getFourthByte() & $other->getFourthByte(),
		);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ip\Ipv4AddressInterface::bitwiseOr()
	 */
	public function bitwiseOr(Ipv4AddressInterface $other) : Ipv4AddressInterface
	{
		return new self(
			$this->getFirstByte() | $other->getFirstByte(),
			$this->getSecondByte() | $other->getSecondByte(),
			$this->getThirdByte() | $other->getThirdByte(),
			$this->getFourthByte() | $other->getFourthByte(),
		);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ip\Ipv4AddressInterface::bitwiseXor()
	 */
	public function bitwiseXor(Ipv4AddressInterface $other) : Ipv4AddressInterface
	{
		return new self(
			$this->getFirstByte() ^ $other->getFirstByte(),
			$this->getSecondByte() ^ $other->getSecondByte(),
			$this->getThirdByte() ^ $other->getThirdByte(),
			$this->getFourthByte() ^ $other->getFourthByte(),
		);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ip\Ipv4AddressInterface::bitwiseNot()
	 */
	public function bitwiseNot() : Ipv4AddressInterface
	{
		return new self(
			~$this->getFirstByte(),
			~$this->getSecondByte(),
			~$this->getThirdByte(),
			~$this->getFourthByte(),
		);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ip\Ipv4AddressInterface::add()
	 */
	public function add(Ipv4AddressInterface $other) : Ipv4AddressInterface
	{
		$ad4 = $this->getFourthByte() + $other->getFourthByte();
		$ad3 = $this->getThirdByte() + $other->getThirdByte() + ($ad4 >> 8);
		$ad2 = $this->getSecondByte() + $other->getSecondByte() + ($ad3 >> 8);
		$ad1 = $this->getFirstByte() + $other->getFirstByte() + ($ad2 >> 8);
		
		return new self($ad1, $ad2, $ad3, $ad4);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ip\Ipv4AddressInterface::substract()
	 */
	public function substract(Ipv4AddressInterface $other) : Ipv4AddressInterface
	{
		$ad1 = $this->getFirstByte() - $other->getFirstByte();
		$ad2 = $this->getSecondByte() - $other->getSecondByte() + ($ad1 << 8);
		$ad3 = $this->getThirdByte() - $other->getThirdByte() + ($ad2 << 8);
		$ad4 = $this->getFourthByte() - $other->getFourthByte() + ($ad3 << 8);
		
		return new self($ad1, $ad2, $ad3, $ad4);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ip\Ipv4AddressInterface::toArray()
	 */
	public function toArray() : array
	{
		return [$this->_b1, $this->_b2, $this->_b3, $this->_b4];
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ip\Ipv4AddressInterface::toIpv6()
	 */
	public function toIpv6() : Ipv6AddressInterface
	{
		return new Ipv6Address(0x64, 0xFF9B, 0, 0, 0, 0, ($this->_b1 << 8) | $this->_b2, ($this->_b3 << 8) | $this->_b4);
	}
	
}
