<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ip-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ip;

/**
 * Ipv6Address class file.
 *
 * This class is a simple implementation of the Ipv6AddressInterface.
 *
 * @author Anastaszor
 */
class Ipv6Address implements Ipv6AddressInterface
{
	
	/**
	 * The first group.
	 * 
	 * @var integer
	 */
	protected int $_b1 = 0;
	
	/**
	 * The second group.
	 * 
	 * @var integer
	 */
	protected int $_b2 = 0;
	
	/**
	 * The third group.
	 * 
	 * @var integer
	 */
	protected int $_b3 = 0;
	
	/**
	 * The fourth group.
	 * 
	 * @var integer
	 */
	protected int $_b4 = 0;
	
	/**
	 * The fifth group.
	 * 
	 * @var integer
	 */
	protected int $_b5 = 0;
	
	/**
	 * The sixth group.
	 * 
	 * @var integer
	 */
	protected int $_b6 = 0;
	
	/**
	 * The seventh group.
	 * 
	 * @var integer
	 */
	protected int $_b7 = 0;
	
	/**
	 * The eighth group.
	 * 
	 * @var integer
	 */
	protected int $_b8 = 0;
	
	/**
	 * Builds a new Ipv6Address with the given byte groups.
	 * 
	 * @param integer $bit1
	 * @param integer $bit2
	 * @param integer $bit3
	 * @param integer $bit4
	 * @param integer $bit5
	 * @param integer $bit6
	 * @param integer $bit7
	 * @param integer $bit8
	 */
	public function __construct(int $bit1, int $bit2, int $bit3, int $bit4, int $bit5, int $bit6, int $bit7, int $bit8)
	{
		$this->_b1 = $bit1 & 0x0000FFFF;
		$this->_b2 = $bit2 & 0x0000FFFF;
		$this->_b3 = $bit3 & 0x0000FFFF;
		$this->_b4 = $bit4 & 0x0000FFFF;
		$this->_b5 = $bit5 & 0x0000FFFF;
		$this->_b6 = $bit6 & 0x0000FFFF;
		$this->_b7 = $bit7 & 0x0000FFFF;
		$this->_b8 = $bit8 & 0x0000FFFF;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return $this->getCanonicalRepresentation();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ip\Ipv6AddressInterface::getFirstGroup()
	 */
	public function getFirstGroup() : int
	{
		return $this->_b1;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ip\Ipv6AddressInterface::getSecondGroup()
	 */
	public function getSecondGroup() : int
	{
		return $this->_b2;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ip\Ipv6AddressInterface::getThirdGroup()
	 */
	public function getThirdGroup() : int
	{
		return $this->_b3;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ip\Ipv6AddressInterface::getFourthGroup()
	 */
	public function getFourthGroup() : int
	{
		return $this->_b4;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ip\Ipv6AddressInterface::getFifthGroup()
	 */
	public function getFifthGroup() : int
	{
		return $this->_b5;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ip\Ipv6AddressInterface::getSixthGroup()
	 */
	public function getSixthGroup() : int
	{
		return $this->_b6;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ip\Ipv6AddressInterface::getSeventhGroup()
	 */
	public function getSeventhGroup() : int
	{
		return $this->_b7;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ip\Ipv6AddressInterface::getEighthGroup()
	 */
	public function getEighthGroup() : int
	{
		return $this->_b8;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ip\Ipv6AddressInterface::getShortRepresentation()
	 */
	public function getShortRepresentation() : string
	{
		$canonical = \dechex($this->_b1)
				.':'.\dechex($this->_b2)
				.':'.\dechex($this->_b3)
				.':'.\dechex($this->_b4)
				.':'.\dechex($this->_b5)
				.':'.\dechex($this->_b6)
				.':'.\dechex($this->_b7)
				.':'.\dechex($this->_b8);
		
		$pos = \mb_strpos($canonical, ':0:0:0:0:0:0:');
		if(false !== $pos)
		{
			return $this->postShort(\substr_replace($canonical, '::', $pos, 13));
		}
		
		$pos = \mb_strpos($canonical, ':0:0:0:0:0:');
		if(false !== $pos)
		{
			return $this->postShort(\substr_replace($canonical, '::', $pos, 11));
		}
		
		$pos = \mb_strpos($canonical, ':0:0:0:0:');
		if(false !== $pos)
		{
			return $this->postShort(\substr_replace($canonical, '::', $pos, 9));
		}
		
		$pos = \mb_strpos($canonical, ':0:0:0:');
		if(false !== $pos)
		{
			return $this->postShort(\substr_replace($canonical, '::', $pos, 7));
		}
		
		$pos = \mb_strpos($canonical, ':0:0:');
		if(false !== $pos)
		{
			return $this->postShort(\substr_replace($canonical, '::', $pos, 5));
		}
		
		$pos = \mb_strpos($canonical, ':0:');
		if(false !== $pos)
		{
			return $this->postShort(\substr_replace($canonical, '::', $pos, 3));
		}
		
		return $canonical;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ip\Ipv6AddressInterface::getCanonicalRepresentation()
	 */
	public function getCanonicalRepresentation() : string
	{
		return \str_pad(\dechex($this->_b1), 4, '0', \STR_PAD_LEFT)
			.':'.\str_pad(\dechex($this->_b2), 4, '0', \STR_PAD_LEFT)
			.':'.\str_pad(\dechex($this->_b3), 4, '0', \STR_PAD_LEFT)
			.':'.\str_pad(\dechex($this->_b4), 4, '0', \STR_PAD_LEFT)
			.':'.\str_pad(\dechex($this->_b5), 4, '0', \STR_PAD_LEFT)
			.':'.\str_pad(\dechex($this->_b6), 4, '0', \STR_PAD_LEFT)
			.':'.\str_pad(\dechex($this->_b7), 4, '0', \STR_PAD_LEFT)
			.':'.\str_pad(\dechex($this->_b8), 4, '0', \STR_PAD_LEFT);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ip\Ipv6AddressInterface::equals()
	 */
	public function equals($object) : bool
	{
		return $object instanceof Ipv6AddressInterface
			&& $this->getFirstGroup() === $object->getFirstGroup()
			&& $this->getSecondGroup() === $object->getSecondGroup()
			&& $this->getThirdGroup() === $object->getThirdGroup()
			&& $this->getFourthGroup() === $object->getFourthGroup()
			&& $this->getFifthGroup() === $object->getFifthGroup()
			&& $this->getSixthGroup() === $object->getSixthGroup()
			&& $this->getSeventhGroup() === $object->getSeventhGroup()
			&& $this->getEighthGroup() === $object->getEighthGroup();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ip\Ipv6AddressInterface::bitwiseAnd()
	 */
	public function bitwiseAnd(Ipv6AddressInterface $other) : Ipv6AddressInterface
	{
		return new self(
			$this->getFirstGroup() & $other->getFirstGroup(),
			$this->getSecondGroup() & $other->getSecondGroup(),
			$this->getThirdGroup() & $other->getThirdGroup(),
			$this->getFourthGroup() & $other->getFourthGroup(),
			$this->getFifthGroup() & $other->getFifthGroup(),
			$this->getSixthGroup() & $other->getSixthGroup(),
			$this->getSeventhGroup() & $other->getSeventhGroup(),
			$this->getEighthGroup() & $other->getEighthGroup(),
		);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ip\Ipv6AddressInterface::bitwiseOr()
	 */
	public function bitwiseOr(Ipv6AddressInterface $other) : Ipv6AddressInterface
	{
		return new self(
			$this->getFirstGroup() | $other->getFirstGroup(),
			$this->getSecondGroup() | $other->getSecondGroup(),
			$this->getThirdGroup() | $other->getThirdGroup(),
			$this->getFourthGroup() | $other->getFourthGroup(),
			$this->getFifthGroup() | $other->getFifthGroup(),
			$this->getSixthGroup() | $other->getSixthGroup(),
			$this->getSeventhGroup() | $other->getSeventhGroup(),
			$this->getEighthGroup() | $other->getEighthGroup(),
		);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ip\Ipv6AddressInterface::bitwiseXor()
	 */
	public function bitwiseXor(Ipv6AddressInterface $other) : Ipv6AddressInterface
	{
		return new self(
			$this->getFirstGroup() ^ $other->getFirstGroup(),
			$this->getSecondGroup() ^ $other->getSecondGroup(),
			$this->getThirdGroup() ^ $other->getThirdGroup(),
			$this->getFourthGroup() ^ $other->getFourthGroup(),
			$this->getFifthGroup() ^ $other->getFifthGroup(),
			$this->getSixthGroup() ^ $other->getSixthGroup(),
			$this->getSeventhGroup() ^ $other->getSeventhGroup(),
			$this->getEighthGroup() ^ $other->getEighthGroup(),
		);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ip\Ipv6AddressInterface::_not()
	 */
	public function bitwiseNot() : Ipv6AddressInterface
	{
		return new self(
			~$this->getFirstGroup(),
			~$this->getSecondGroup(),
			~$this->getThirdGroup(),
			~$this->getFourthGroup(),
			~$this->getFifthGroup(),
			~$this->getSixthGroup(),
			~$this->getSeventhGroup(),
			~$this->getEighthGroup(),
		);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ip\Ipv6AddressInterface::add()
	 */
	public function add(Ipv6AddressInterface $other) : Ipv6AddressInterface
	{
		$ad8 = $this->getEighthGroup() + $other->getEighthGroup();
		$ad7 = $this->getSeventhGroup() + $other->getSeventhGroup() + ($ad8 >> 16);
		$ad6 = $this->getSixthGroup() + $other->getSixthGroup() + ($ad7 >> 16);
		$ad5 = $this->getFifthGroup() + $other->getFifthGroup() + ($ad6 >> 16);
		$ad4 = $this->getFourthGroup() + $other->getFourthGroup() + ($ad5 >> 16);
		$ad3 = $this->getThirdGroup() + $other->getThirdGroup() + ($ad4 >> 16);
		$ad2 = $this->getSecondGroup() + $other->getSecondGroup() + ($ad3 >> 16);
		$ad1 = $this->getFirstGroup() + $other->getFirstGroup() + ($ad2 >> 16);
		
		return new self($ad1, $ad2, $ad3, $ad4, $ad5, $ad6, $ad7, $ad8);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ip\Ipv6AddressInterface::substract()
	 */
	public function substract(Ipv6AddressInterface $other) : Ipv6AddressInterface
	{
		$ad1 = $this->getFirstGroup() - $other->getFirstGroup();
		$ad2 = $this->getSecondGroup() - $other->getSecondGroup() + ($ad1 << 16);
		$ad3 = $this->getThirdGroup() - $other->getThirdGroup() + ($ad2 << 16);
		$ad4 = $this->getFourthGroup() - $other->getFourthGroup() + ($ad3 << 16);
		$ad5 = $this->getFifthGroup() - $other->getFifthGroup() + ($ad4 << 16);
		$ad6 = $this->getSixthGroup() - $other->getSixthGroup() + ($ad5 << 16);
		$ad7 = $this->getSeventhGroup() - $other->getSeventhGroup() + ($ad6 << 16);
		$ad8 = $this->getEighthGroup() - $other->getEighthGroup() + ($ad7 << 16);
		
		return new self($ad1, $ad2, $ad3, $ad4, $ad5, $ad6, $ad7, $ad8);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ip\Ipv6AddressInterface::toArray()
	 */
	public function toArray() : array
	{
		return [$this->_b1, $this->_b2, $this->_b3, $this->_b4, $this->_b5, $this->_b6, $this->_b7, $this->_b8];
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ip\Ipv6AddressInterface::toIpv4()
	 */
	public function toIpv4() : Ipv4AddressInterface
	{
		return new Ipv4Address($this->_b7 >> 8, $this->_b7, $this->_b8 >> 8, $this->_b8);
	}
	
	/**
	 * Removes beginning and trailing zeroes if any.
	 * 
	 * @param string $ipaddr
	 * @return string the shortened ip
	 */
	protected function postShort(string $ipaddr) : string
	{
		$begin = \mb_strpos($ipaddr, '0::');
		if(0 === $begin)
		{
			$ipaddr = (string) \mb_substr($ipaddr, 1);
		}
		
		$end = \mb_strrpos($ipaddr, '::0');
		if((int) \mb_strlen($ipaddr) - 3 === $end)
		{
			$ipaddr = (string) \mb_substr($ipaddr, 0, -1);
		}
		
		return $ipaddr;
	}
	
}
