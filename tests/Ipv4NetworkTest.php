<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ip-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Ip\Ipv4Address;
use PhpExtended\Ip\Ipv4Network;
use PHPUnit\Framework\TestCase;

/**
 * Ipv4NetworkTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Ip\Ipv4Network
 *
 * @internal
 *
 * @small
 */
class Ipv4NetworkTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var Ipv4Network
	 */
	protected Ipv4Network $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('192.168.16.0/20', $this->_object->__toString());
	}
	
	public function testGetStartIp() : void
	{
		$this->assertEquals(new Ipv4Address(192, 168, 16, 0), $this->_object->getStartIp());
	}
	
	public function testGetEndIp() : void
	{
		$this->assertEquals(new Ipv4Address(192, 168, 31, 255), $this->_object->getEndIp());
	}
	
	public function testGetNetworkIp() : void
	{
		$this->assertEquals(new Ipv4Address(192, 168, 16, 0), $this->_object->getNetworkIp());
	}
	
	public function testGetNetmaskIp() : void
	{
		$this->assertEquals(new Ipv4Address(255, 255, 240, 0), $this->_object->getNetmaskIp());
	}
	
	public function testGetWildmaskIp() : void
	{
		$this->assertEquals(new Ipv4Address(0, 0, 15, 255), $this->_object->getWildmaskIp());
	}
	
	public function testGetMaskBits() : void
	{
		$this->assertEquals(20, $this->_object->getMaskBits());
	}
	
	public function testGetGatewayIp() : void
	{
		$this->assertEquals(new Ipv4Address(192, 168, 16, 1), $this->_object->getGatewayIp());
	}
	
	public function testGetBroadcastIp() : void
	{
		$this->assertEquals(new Ipv4Address(192, 168, 31, 254), $this->_object->getBroadcastIp());
	}
	
	public function testGetNumberOfAddresses() : void
	{
		$this->assertEquals(4094, $this->_object->getNumberOfAddresses());
	}
	
	public function testGetNetworkClassA() : void
	{
		$this->assertEquals('A', (new Ipv4Network(new Ipv4Address(10, 0, 0, 1), 10))->getNetworkClass());
	}
	
	public function testGetNetworkClassB() : void
	{
		$this->assertEquals('B', (new Ipv4Network(new Ipv4Address(172, 0, 0, 1), 10))->getNetworkClass());
	}
	
	public function testGetNetworkClassC() : void
	{
		$this->assertEquals('C', $this->_object->getNetworkClass());
	}
	
	public function testGetNetworkClassD() : void
	{
		$this->assertEquals('D', (new Ipv4Network(new Ipv4Address(224, 0, 0, 1), 10))->getNetworkClass());
	}
	
	public function testGetNetworkClassE() : void
	{
		$this->assertEquals('E', (new Ipv4Network(new Ipv4Address(250, 0, 0, 1), 10))->getNetworkClass());
	}
	
	public function testGetEquals() : void
	{
		$this->assertTrue($this->_object->equals($this->_object));
	}
	
	public function testContainsAddress() : void
	{
		$this->assertTrue($this->_object->containsAddress(new Ipv4Address(192, 168, 30, 23)));
	}
	
	public function testContainsNetwork() : void
	{
		$this->assertTrue($this->_object->containsNetwork(new Ipv4Network(new Ipv4Address(192, 168, 23, 76), 24)));
	}
	
	public function testAbsorbAddressWithinRange() : void
	{
		$this->assertEquals($this->_object, $this->_object->absorbAddress(new Ipv4Address(192, 168, 30, 23)));
	}
	
	public function testAbsorbAddressOutsideRange() : void
	{
		$this->assertEquals(new Ipv4Network(new Ipv4Address(0, 0, 0, 0), 0), $this->_object->absorbAddress(new Ipv4Address(10, 0, 0, 1)));
	}
	
	public function testAbsorbNetwork() : void
	{
		$this->assertEquals(new Ipv4Network(new Ipv4Address(192, 168, 0, 0), 19), $this->_object->absorbNetwork(new Ipv4Network(new Ipv4Address(192, 168, 0, 1), 23)));
	}
	
	public function testGetCanonicalRepresentation() : void
	{
		$this->assertEquals('192.168.16.0/20', $this->_object->getCanonicalRepresentation());
	}
	
	public function testToArray() : void
	{
		$this->assertEquals([192, 168, 16, 0, 20], $this->_object->toArray());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new Ipv4Network(new Ipv4Address(192, 168, 23, 76), 20);
	}
	
}
