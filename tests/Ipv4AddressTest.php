<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ip-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Ip\Ipv4Address;
use PhpExtended\Ip\Ipv6Address;
use PHPUnit\Framework\TestCase;

/**
 * Ipv4AddressTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Ip\Ipv4Address
 *
 * @internal
 *
 * @small
 */
class Ipv4AddressTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var Ipv4Address
	 */
	protected Ipv4Address $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('192.168.23.76', $this->_object->__toString());
	}
	
	public function testGetFirstByte() : void
	{
		$this->assertEquals(192, $this->_object->getFirstByte());
	}
	
	public function testGetSecondByte() : void
	{
		$this->assertEquals(168, $this->_object->getSecondByte());
	}
	
	public function testGetThirdByte() : void
	{
		$this->assertEquals(23, $this->_object->getThirdByte());
	}
	
	public function testGetFourthByte() : void
	{
		$this->assertEquals(76, $this->_object->getFourthByte());
	}
	
	public function testGetSignedValue() : void
	{
		$this->assertEquals(3232241484, $this->_object->getSignedValue());
	}
	
	public function testGetShortRepresentation() : void
	{
		$this->assertEquals('192.168.23.76', $this->_object->getShortRepresentation());
	}
	
	public function testGetCanonicalRepresentation() : void
	{
		$this->assertEquals('192.168.23.76', $this->_object->getCanonicalRepresentation());
	}
	
	public function testEquals() : void
	{
		$this->assertTrue($this->_object->equals($this->_object));
	}
	
	public function testBitwiseAnd() : void
	{
		$this->assertEquals(new Ipv4Address(64, 32, 6, 68), $this->_object->bitwiseAnd(new Ipv4Address(102, 102, 102, 102)));
	}
	
	public function testBitwiseOr() : void
	{
		$this->assertEquals(new Ipv4Address(230, 238, 119, 110), $this->_object->bitwiseOr(new Ipv4Address(102, 102, 102, 102)));
	}
	
	public function testBitwiseXor() : void
	{
		$this->assertEquals(new Ipv4Address(166, 206, 113, 42), $this->_object->bitwiseXor(new Ipv4Address(102, 102, 102, 102)));
	}
	
	public function testBitwiseNot() : void
	{
		$this->assertEquals(new Ipv4Address(63, 87, 232, 179), $this->_object->bitwiseNot());
	}
	
	public function testAdd() : void
	{
		$this->assertEquals(new Ipv4Address(193, 170, 26, 80), $this->_object->add(new Ipv4Address(1, 2, 3, 4)));
	}
	
	public function testSubtract() : void
	{
		$this->assertEquals(new Ipv4Address(191, 166, 20, 72), $this->_object->substract(new Ipv4Address(1, 2, 3, 4)));
	}
	
	public function testToArray() : void
	{
		$this->assertEquals([192, 168, 23, 76], $this->_object->toArray());
	}
	
	public function testToIpv6() : void
	{
		$this->assertEquals(new Ipv6Address(100, 65435, 0, 0, 0, 0, 49320, 5964), $this->_object->toIpv6());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new Ipv4Address(192, 168, 23, 76);
	}
	
}
