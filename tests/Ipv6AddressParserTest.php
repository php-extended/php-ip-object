<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ip-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Ip\Ipv6Address;
use PhpExtended\Ip\Ipv6AddressParser;
use PhpExtended\Parser\ParseException;
use PHPUnit\Framework\TestCase;

/**
 * Ipv6AddressParserTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Ip\Ipv6AddressParser
 *
 * @internal
 *
 * @small
 */
class Ipv6AddressParserTest extends TestCase
{
	
	/**
	 * The parser to test.
	 * 
	 * @var Ipv6AddressParser
	 */
	protected Ipv6AddressParser $_parser;
	
	public function testNull() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_parser->parse(null);
	}
	
	public function testLocalhost() : void
	{
		$this->assertEquals(new Ipv6Address(0, 0, 0, 0, 0, 0, 0, 1), $this->_parser->parse('localhost'));
	}
	
	public function testZero() : void
	{
		$this->assertEquals(new Ipv6Address(0, 0, 0, 0, 0, 0, 0, 0), $this->_parser->parse('0:0:0:0:0:0:0:0'));
	}
	
	public function testBeginNil() : void
	{
		$this->assertEquals(new Ipv6Address(0, 0, 0, 0, 0, 0, 0, 0), $this->_parser->parse('::'));
	}
	
	public function testBeginShort() : void
	{
		$this->assertEquals(new Ipv6Address(0, 0, 0, 0, 0, 0, 0, 1), $this->_parser->parse('::1'));
	}
	
	public function testEndinShort() : void
	{
		$this->assertEquals(new Ipv6Address(0xFE80, 0, 0, 0, 0, 0, 0, 0), $this->_parser->parse('fe80::'));
	}
	
	public function testShort() : void
	{
		$this->assertEquals(new Ipv6Address(0xFE80, 0, 0, 0, 0, 0, 0x1234, 0x5678), $this->_parser->parse('fe80::1234:5678'));
	}
	
	public function testNotEnough() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_parser->parse('1234:5678');
	}
	
	public function testTooMuch() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_parser->parse('1234:5678:90ab:cdef:1234:5678:90ab:cdef:1234');
	}
	
	public function testNetwork() : void
	{
		$this->assertEquals(new Ipv6Address(0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF), $this->_parser->parse('ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff'));
	}
	
	public function testTooHigh() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_parser->parse('1234:4567:7890a::');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_parser = new Ipv6AddressParser();
	}
	
}
