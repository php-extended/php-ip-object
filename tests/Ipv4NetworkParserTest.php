<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ip-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Ip\Ipv4Address;
use PhpExtended\Ip\Ipv4Network;
use PhpExtended\Ip\Ipv4NetworkParser;
use PhpExtended\Parser\ParseException;
use PHPUnit\Framework\TestCase;

/**
 * Ipv4NetworkParserTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Ip\Ipv4NetworkParser
 *
 * @internal
 *
 * @small
 */
class Ipv4NetworkParserTest extends TestCase
{
	
	/**
	 * The parser to test.
	 * 
	 * @var Ipv4NetworkParser
	 */
	protected Ipv4NetworkParser $_parser;
	
	public function testNull() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_parser->parse(null);
	}
	
	public function testLocalhost() : void
	{
		$this->assertEquals(new Ipv4Network(new Ipv4Address(127, 0, 0, 1), 32), $this->_parser->parse('localhost'));
	}
	
	public function testZero() : void
	{
		$this->assertEquals(new Ipv4Network(new Ipv4Address(0, 0, 0, 0), 0), $this->_parser->parse('0.0.0.0/0'));
	}
	
	public function testNotEnough() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_parser->parse('1.2.3/20');
	}
	
	public function testTooMuch() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_parser->parse('1.2.3.4.5/20');
	}
	
	public function testAddress() : void
	{
		$this->assertEquals(new Ipv4Network(new Ipv4Address(192, 168, 0, 0), 16), $this->_parser->parse('192.168.0.1/16'));
	}
	
	public function testNetwork() : void
	{
		$this->assertEquals(new Ipv4Network(new Ipv4Address(255, 255, 255, 255), 32), $this->_parser->parse('255.255.255.255/32'));
	}
	
	public function testTooHigh() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_parser->parse('127.12.24.512/20');
	}
	
	public function testMaskTooHigh() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_parser->parse('127.12.34.56/78');
	}
	
	public function testMissingMask() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_parser->parse('127.12.34.56');
	}
	
	public function testMissingMaskValue() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_parser->parse('127.12.34.56/');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_parser = new Ipv4NetworkParser();
	}
	
}
