<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ip-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Ip\Ipv6Address;
use PhpExtended\Ip\Ipv6Network;
use PhpExtended\Ip\Ipv6NetworkParser;
use PhpExtended\Parser\ParseException;
use PHPUnit\Framework\TestCase;

/**
 * Ipv6NetworkParserTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Ip\Ipv6NetworkParser
 *
 * @internal
 *
 * @small
 */
class Ipv6NetworkParserTest extends TestCase
{
	
	/**
	 * The parser to test.
	 * 
	 * @var Ipv6NetworkParser
	 */
	protected Ipv6NetworkParser $_parser;
	
	public function testNull() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_parser->parse(null);
	}
	
	public function testLocalhost() : void
	{
		$this->assertEquals(new Ipv6Network(new Ipv6Address(0, 0, 0, 0, 0, 0, 0, 1), 128), $this->_parser->parse('localhost'));
	}
	
	public function testZero() : void
	{
		$this->assertEquals(new Ipv6Network(new Ipv6Address(0, 0, 0, 0, 0, 0, 0, 0), 0), $this->_parser->parse('0:0:0:0:0:0:0:0/0'));
	}
	
	public function testBeginNil() : void
	{
		$this->assertEquals(new Ipv6Network(new Ipv6Address(0, 0, 0, 0, 0, 0, 0, 0), 128), $this->_parser->parse('::/128'));
	}
	
	public function testBeginShort() : void
	{
		$this->assertEquals(new Ipv6Network(new Ipv6Address(0, 0, 0, 0, 0, 0, 0, 1), 128), $this->_parser->parse('::1/128'));
	}
	
	public function testEndinShort() : void
	{
		$this->assertEquals(new Ipv6Network(new Ipv6Address(0xFE80, 0, 0, 0, 0, 0, 0, 0), 16), $this->_parser->parse('fe80::/16'));
	}
	
	public function testAddress() : void
	{
		$this->assertEquals(new Ipv6Network(new Ipv6Address(0xFE80, 0, 0, 0, 0, 0, 0, 0x1234), 128), $this->_parser->parse('fe80::1234/128'));
	}
	
	public function testNotEnough() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_parser->parse('1234:5678/64');
	}
	
	public function testTooMuch() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_parser->parse('1234:5678:90ab:cdef:1234:5678:90ab:cdef:1234/120');
	}
	
	public function testNetwork() : void
	{
		$this->assertEquals(new Ipv6Network(new Ipv6Address(0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF), 128), $this->_parser->parse('ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff/128'));
	}
	
	public function testTooHigh() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_parser->parse('1234:5678:90abc::/20');
	}
	
	public function testMaskTooHigh() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_parser->parse('fe80::/256');
	}
	
	public function testMissingMask() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_parser->parse('fe80::');
	}
	
	public function testMissingMaskValue() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_parser->parse('fe80::/');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_parser = new Ipv6NetworkParser();
	}
	
}
