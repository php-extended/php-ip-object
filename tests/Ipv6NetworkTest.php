<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ip-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Ip\Ipv6Address;
use PhpExtended\Ip\Ipv6Network;
use PHPUnit\Framework\TestCase;

/**
 * Ipv6NetworkTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Ip\Ipv6Network
 *
 * @internal
 *
 * @small
 */
class Ipv6NetworkTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var Ipv6Network
	 */
	protected Ipv6Network $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('1234:5678:9abc:def0:0001:0000:f000:0000/100', $this->_object->__toString());
	}
	
	public function testGetStartIp() : void
	{
		$this->assertEquals(new Ipv6Address(0x1234, 0x5678, 0x9ABC, 0xDEF0, 1, 0, 0xF000, 0), $this->_object->getStartIp());
	}
	
	public function testGetEndIp() : void
	{
		$this->assertEquals(new Ipv6Address(0x1234, 0x5678, 0x9ABC, 0xDEF0, 1, 0, 0xFFFF, 0xFFFF), $this->_object->getEndIp());
	}
	
	public function testGetNetworkIp() : void
	{
		$this->assertEquals(new Ipv6Address(0x1234, 0x5678, 0x9ABC, 0xDEF0, 1, 0, 0xF000, 0), $this->_object->getNetworkIp());
	}
	
	public function testGetNetmaskIp() : void
	{
		$this->assertEquals(new Ipv6Address(0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xF000, 0), $this->_object->getNetmaskIp());
	}
	
	public function testGetWildmaskIp() : void
	{
		$this->assertEquals(new Ipv6Address(0, 0, 0, 0, 0, 0, 0xFFF, 0xFFFF), $this->_object->getWildmaskIp());
	}
	
	public function testGetMaskBits() : void
	{
		$this->assertEquals(100, $this->_object->getMaskBits());
	}
	
	public function testGetGatewayIp() : void
	{
		$this->assertEquals(new Ipv6Address(0x1234, 0x5678, 0x9ABC, 0xDEF0, 1, 0, 0xF000, 1), $this->_object->getGatewayIp());
	}
	
	public function testGetBroadcastIp() : void
	{
		$this->assertEquals(new Ipv6Address(0x1234, 0x5678, 0x9ABC, 0xDEF0, 1, 0, 0xFFFF, 0xFFFE), $this->_object->getBroadcastIp());
	}
	
	public function testGetNumberOfAddresses() : void
	{
		$this->assertEquals(268435454, $this->_object->getNumberOfAddresses());
	}
	
	public function testEquals() : void
	{
		$this->assertTrue($this->_object->equals($this->_object));
	}
	
	public function testContainsAddress() : void
	{
		$this->assertTrue($this->_object->containsAddress(new Ipv6Address(0x1234, 0x5678, 0x9ABC, 0xDEF0, 1, 0, 0xFCFF, 0x1234)));
	}
	
	public function testContainsNetwork() : void
	{
		$this->assertTrue($this->_object->containsNetwork(new Ipv6Network(new Ipv6Address(0x1234, 0x5678, 0x9ABC, 0xDEF0, 1, 0, 0xFF56, 0x5647), 101)));
	}
	
	public function testAbsorbAddressInside() : void
	{
		$this->assertEquals($this->_object, $this->_object->absorbAddress(new Ipv6Address(0x1234, 0x5678, 0x9ABC, 0xDEF0, 1, 0, 0xF456, 0x1234)));
	}
	
	public function testAbsorbAddressOutside() : void
	{
		$this->assertEquals(new Ipv6Network(new Ipv6Address(0x1234, 0x5678, 0x9ABC, 0xDEF0, 0, 0, 0, 0), 79), $this->_object->absorbAddress(new Ipv6Address(0x1234, 0x5678, 0x9ABC, 0xDEF0, 0, 0, 1, 2)));
	}
	
	public function testAbsorbNetwork() : void
	{
		$this->assertEquals(new Ipv6Network(new Ipv6Address(0x1234, 0x5678, 0x9ABC, 0xDEF0, 0, 0, 0, 0), 79), $this->_object->absorbNetwork(new Ipv6Network(new Ipv6Address(0x1234, 0x5678, 0x9ABC, 0xDEF0, 0, 0x1234, 0x5678, 0xABCD), 100)));
	}
	
	public function testGetCanonicalRepresentation() : void
	{
		$this->assertEquals('1234:5678:9abc:def0:0001:0000:f000:0000/100', $this->_object->getCanonicalRepresentation());
	}
	
	public function testToArray() : void
	{
		$this->assertEquals([0x1234, 0x5678, 0x9ABC, 0xDEF0, 1, 0, 0xF000, 0, 100], $this->_object->toArray());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$ipAddress = new Ipv6Address(0x1234, 0x5678, 0x9ABC, 0xDEF0, 1, 0, 0xFCA8, 0x6420);
		$this->_object = new Ipv6Network($ipAddress, 100);
	}
	
}
