<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ip-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Ip\Ipv4Address;
use PhpExtended\Ip\Ipv4AddressParser;
use PhpExtended\Parser\ParseException;
use PHPUnit\Framework\TestCase;

/**
 * Ipv4AddressParserTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Ip\Ipv4AddressParser
 *
 * @internal
 *
 * @small
 */
class Ipv4AddressParserTest extends TestCase
{
	
	/**
	 * The parser to test.
	 * 
	 * @var Ipv4AddressParser
	 */
	protected Ipv4AddressParser $_parser;
	
	public function testNull() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_parser->parse(null);
	}
	
	public function testLocalhost() : void
	{
		$this->assertEquals(new Ipv4Address(127, 0, 0, 1), $this->_parser->parse('localhost'));
	}
	
	public function testZero() : void
	{
		$this->assertEquals(new Ipv4Address(0, 0, 0, 0), $this->_parser->parse('0.0.0.0'));
	}
	
	public function testNotEnough() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_parser->parse('1.2.3');
	}
	
	public function testTooMuch() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_parser->parse('1.2.3.4.5');
	}
	
	public function testAddress() : void
	{
		$this->assertEquals(new Ipv4Address(12, 34, 56, 78), $this->_parser->parse('12.34.56.78'));
	}
	
	public function testNetwork() : void
	{
		$this->assertEquals(new Ipv4Address(255, 255, 255, 255), $this->_parser->parse('255.255.255.255'));
	}
	
	public function testTooHigh() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_parser->parse('127.0.0.512');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_parser = new Ipv4AddressParser();
	}
	
}
