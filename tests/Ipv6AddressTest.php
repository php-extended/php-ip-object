<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ip-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Ip\Ipv4Address;
use PhpExtended\Ip\Ipv6Address;
use PHPUnit\Framework\TestCase;

/**
 * Ipv6AddressTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Ip\Ipv6Address
 *
 * @internal
 *
 * @small
 */
class Ipv6AddressTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var Ipv6Address
	 */
	protected Ipv6Address $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('1234:5678:9abc:def0:0001:0000:fca8:6420', $this->_object->__toString());
	}
	
	public function testGetFirstGroup() : void
	{
		$this->assertEquals(0x1234, $this->_object->getFirstGroup());
	}
	
	public function testGetSecondGroup() : void
	{
		$this->assertEquals(0x5678, $this->_object->getSecondGroup());
	}
	
	public function testGetThirdGroup() : void
	{
		$this->assertEquals(0x9ABC, $this->_object->getThirdGroup());
	}
	
	public function testGetFourthGroup() : void
	{
		$this->assertEquals(0xDEF0, $this->_object->getFourthGroup());
	}
	
	public function testGetFifthGroup() : void
	{
		$this->assertEquals(1, $this->_object->getFifthGroup());
	}
	
	public function testGetSixthGroup() : void
	{
		$this->assertEquals(0, $this->_object->getSixthGroup());
	}
	
	public function testGetSeventhGroup() : void
	{
		$this->assertEquals(0xFCA8, $this->_object->getSeventhGroup());
	}
	
	public function testGetEighthGroup() : void
	{
		$this->assertEquals(0x6420, $this->_object->getEighthGroup());
	}
	
	public function testGetShortRepresentation() : void
	{
		$this->assertEquals('1234:5678:9abc:def0:1::fca8:6420', $this->_object->getShortRepresentation());
	}
	
	public function testGetShortRepresentation00() : void
	{
		$this->assertEquals('::', (new Ipv6Address(0, 0, 0, 0, 0, 0, 0, 0))->getShortRepresentation());
	}
	
	public function testGetShortRepresentation01() : void
	{
		$this->assertEquals('::1', (new Ipv6Address(0, 0, 0, 0, 0, 0, 0, 1))->getShortRepresentation());
	}
	
	public function testGetShortRepresentation02() : void
	{
		$this->assertEquals('::1:1', (new Ipv6Address(0, 0, 0, 0, 0, 0, 1, 1))->getShortRepresentation());
	}
	
	public function testGetShortRepresentation03() : void
	{
		$this->assertEquals('::1:1:1', (new Ipv6Address(0, 0, 0, 0, 0, 1, 1, 1))->getShortRepresentation());
	}
	
	public function testGetShortRepresentation04() : void
	{
		$this->assertEquals('::1:1:1:1', (new Ipv6Address(0, 0, 0, 0, 1, 1, 1, 1))->getShortRepresentation());
	}
	
	public function testGetShortRepresentation05() : void
	{
		$this->assertEquals('::1:1:1:1:1', (new Ipv6Address(0, 0, 0, 1, 1, 1, 1, 1))->getShortRepresentation());
	}
	
	public function testGetShortRepresentation06() : void
	{
		$this->assertEquals('::1:1:1:1:1:1', (new Ipv6Address(0, 0, 1, 1, 1, 1, 1, 1))->getShortRepresentation());
	}
	
	public function testGetShortRepresentation07() : void
	{
		$this->assertEquals('0:1:1:1:1:1:1:1', (new Ipv6Address(0, 1, 1, 1, 1, 1, 1, 1))->getShortRepresentation());
	}
	
	public function testGetShortRepresentationFull() : void
	{
		$this->assertEquals('1:1:1:1:1:1:1:1', (new Ipv6Address(1, 1, 1, 1, 1, 1, 1, 1))->getShortRepresentation());
	}
	
	public function testGetCanonicalRepresentation() : void
	{
		$this->assertEquals('1234:5678:9abc:def0:0001:0000:fca8:6420', $this->_object->getCanonicalRepresentation());
	}
	
	public function testEquals() : void
	{
		$this->assertTrue($this->_object->equals($this->_object));
	}
	
	public function testBitwiseAnd() : void
	{
		$this->assertEquals(new Ipv6Address(0x34, 0x78, 0xBC, 0xF0, 1, 0, 0xA8, 0x20), $this->_object->bitwiseAnd(new Ipv6Address(255, 255, 255, 255, 255, 255, 255, 255)));
	}
	
	public function testBitwiseOr() : void
	{
		$this->assertEquals(new Ipv6Address(0x12FF, 0x56FF, 0x9AFF, 0xDEFF, 255, 255, 0xFCFF, 0x64FF), $this->_object->bitwiseOr(new Ipv6Address(255, 255, 255, 255, 255, 255, 255, 255)));
	}
	
	public function testBitwiseXor() : void
	{
		$this->assertEquals(new Ipv6Address(0x12CB, 0x5687, 0x9A43, 0xDE0F, 254, 255, 0xFC57, 0x64DF), $this->_object->bitwiseXor(new Ipv6Address(255, 255, 255, 255, 255, 255, 255, 255)));
	}
	
	public function testBitwiseNot() : void
	{
		$this->assertEquals(new Ipv6Address(0xEDCB, 0xA987, 0x6543, 0x210F, 0xFFFE, 0xFFFF, 0x0357, 0x9BDF), $this->_object->bitwiseNot());
	}
	
	public function testAdd() : void
	{
		$this->assertEquals(new Ipv6Address(0x1235, 0x567A, 0x9ABF, 0xDEF4, 6, 6, 0xFCAF, 0x6428), $this->_object->add(new Ipv6Address(1, 2, 3, 4, 5, 6, 7, 8)));
	}
	
	public function testSubtract() : void
	{
		$this->assertEquals(new Ipv6Address(0x1233, 0x5676, 0x9AB9, 0xDEEC, 1, 0, 0xFCA1, 0x6418), $this->_object->substract(new Ipv6Address(1, 2, 3, 4, 0, 0, 7, 8)));
	}
	
	public function testToArray() : void
	{
		$this->assertEquals([0x1234, 0x5678, 0x9ABC, 0xDEF0, 1, 0, 0xFCA8, 0x6420], $this->_object->toArray());
	}
	
	public function testToIpv4() : void
	{
		$this->assertEquals(new Ipv4Address(0xFC, 0xA8, 0x64, 0x20), $this->_object->toIpv4());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new Ipv6Address(0x1234, 0x5678, 0x9ABC, 0xDEF0, 1, 0, 0xFCA8, 0x6420);
	}
	
}
