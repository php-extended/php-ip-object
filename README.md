# php-extended/php-ip-object

A library that implements the php-extended/php-ip-interface interface library.

![coverage](https://gitlab.com/php-extended/php-ip-object/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-ip-object/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-ip-object ^8`


## Basic Usage

You may use this library this following way:

```php

use PhpExtended\Ip\Ipv4;

$ipv4 = new Ipv4(255, 255, 255, 255);
```

The same process is usable with the `Ipv6` class, for version 6 of IP
protocol. This library provides also network classes to be able to evaluate
if a specific ip address is within a network.

To parse ip addresses, do :

```php

use PhpExtended\Ip\Ipv4AddressParser;
use PhpExtended\Ip\IpAddressParseException;

$parser = new Ipv4AddressParser();

try
{
	$ipv4 = $parser->parse("255.255.255.0");
}
catch(IpAddressParseException $e)
{
	// do something
}
```

The same process is usable with the `Ipv6` class, for version 6 of IP
protocol. This library provides also network classes to be able to evaluate
if a specific ip address is within a network.


`/!\` This library does not support embedded ipv4 in ipv6 addresses `/!\`


## License

MIT (See [license file](LICENSE)).
